package konfig

import (
	"bufio"
	"fmt"
	"io"
	"reflect"
	"strings"
)

func Unmarshal(dst Config, r io.Reader) error {
	Init(dst)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		eqi := strings.IndexByte(line, '=')
		if eqi == -1 {
			continue
		}
		path, value := strings.TrimSpace(line[:eqi]), strings.TrimSpace(line[eqi+1:])
		if path == "" || strings.HasPrefix(path, "#") {
			continue
		}
		dst.SetValue(strings.ToLower(path), value, true, false)
	}
	return scanner.Err()
}

func Marshal(src Config, w io.Writer) error {
	Init(src)
	first, prevPrefix := true, ""
	return src.ScanValues(true, func(path, desc, value, defaultValue string, typ reflect.Type) error {
		prefix := ""
		if dot := strings.LastIndexByte(path, '.'); dot != -1 {
			prefix = path[:dot]
		}
		if prefix != prevPrefix && !first {
			fmt.Fprint(w, "\n")
		}
		first = false
		prevPrefix = prefix

		if desc != "" && defaultValue != "" {
			fmt.Fprintf(w, "# %s. Default: %s\n", desc, defaultValue)
		} else if defaultValue != "" {
			fmt.Fprintf(w, "# default: %s\n", defaultValue)
		} else if desc != "" {
			fmt.Fprintf(w, "# %s\n", desc)
		}
		_, err := fmt.Fprintf(w, "%s = %s\n", path, value)
		return err
	})
}
