package konfig

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFieldAES(t *testing.T) {
	f := NewAES("123", "")

	assert.Equal(t, "123", f.String())
	assert.Equal(t, "123", f.GetString(false))
	assert.Equal(t, "123", f.GetString(true))

	f.SetString("213", true)
	assert.Equal(t, "213", f.GetString(false))

	f = NewAES("123", "098nct5089ct98ctuc98m-5c598m2c9f")

	assert.Equal(t, "123", f.String())
	assert.Equal(t, "123", f.GetString(false))

	assert.Equal(t, 44, len(f.GetString(true)))
}
