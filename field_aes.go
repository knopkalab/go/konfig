package konfig

import "gitlab.com/knopkalab/go/utils"

var _ Field = (*AES)(nil)

type AES struct {
	Key string // need len 16 or 32
	Val string
}

func NewAES(val, key string) *AES {
	return &AES{Key: key, Val: val}
}

func (f *AES) String() string { return f.Val }

func (f *AES) GetString(converted bool) string {
	switch {
	case f.Val == "":
		return ""
	case converted && f.Key != "":
		cryptor, err := utils.NewAES(f.Key)
		if err != nil {
			panic(err)
		}
		return cryptor.EncryptToBase64(f.Val)
	default:
		return f.Val
	}
}

func (f *AES) SetString(value string, converted bool) {
	if converted && f.Key != "" && value != "" {
		cryptor, err := utils.NewAES(f.Key)
		if err != nil {
			panic(err)
		}
		if decrypted, err := cryptor.DecryptFromBase64(value); err == nil {
			value = decrypted
		}
	}
	f.Val = value
}
