package konfig

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

var _ Field = (*Duration)(nil)

// Duration pretty type based on time.Duration
type Duration struct{ time.Duration }

func NewDuration(d time.Duration) Duration { return Duration{d} }

const durationDay = time.Hour * 24

// String returns (without spaces) "- 0d 0h 0m 0s 0ms 0us 0ns"
func (dt Duration) String() string {
	dur := dt.Duration
	if dur == 0 {
		return "0s"
	}
	var buf strings.Builder
	if dur < 0 {
		buf.WriteByte('-')
		dur = -dur
	}
	d := int(dur / durationDay)
	h := int((dur % durationDay) / time.Hour)
	m := int((dur % time.Hour) / time.Minute)
	s := int((dur % time.Minute) / time.Second)
	ms := int((dur % time.Second) / time.Millisecond)
	us := int((dur % time.Millisecond) / time.Microsecond)
	ns := int((dur % time.Microsecond) / time.Nanosecond)
	if d != 0 {
		buf.WriteString(strconv.Itoa(d))
		buf.WriteByte('d')
	}
	if h != 0 {
		buf.WriteString(strconv.Itoa(h))
		buf.WriteByte('h')
	}
	if m != 0 {
		buf.WriteString(strconv.Itoa(m))
		buf.WriteByte('m')
	}
	if s != 0 {
		buf.WriteString(strconv.Itoa(s))
		buf.WriteByte('s')
	}
	if ms != 0 {
		buf.WriteString(strconv.Itoa(ms))
		buf.WriteString("ms")
	}
	if us != 0 {
		buf.WriteString(strconv.Itoa(us))
		buf.WriteString("us")
	}
	if ns != 0 {
		buf.WriteString(strconv.Itoa(ns))
		buf.WriteString("ns")
	}
	return buf.String()
}

func (dt Duration) GetString(converted bool) string { return dt.String() }

// SetString "- 0d 0h 0m 0s 0ms 0us 0ns"
func (dt *Duration) SetString(value string, converted bool) {
	if d, ok := parseDuration(value); ok {
		dt.Duration = d
	}
}

func (dt Duration) Seconds() int {
	return int(dt.Duration / time.Second)
}

var (
	rxDurationNum  = regexp.MustCompile(`(\d+)`)
	rxDurationAlph = regexp.MustCompile(`([a-z]+)`)
)

// ParseDuration string "- 0d 0h 0m 0s 0ms 0us 0ns"
func ParseDuration(s string) (d Duration, ok bool) {
	dur, ok := parseDuration(s)
	return NewDuration(dur), ok
}

func parseDuration(s string) (d time.Duration, ok bool) {
	s = strings.ToLower(s)
	numbers := rxDurationNum.FindAllString(s, -1)
	markers := rxDurationAlph.FindAllString(s, -1)
	if len(numbers) == 1 && len(markers) == 0 {
		markers = []string{"s"}
	} else if len(numbers) == 0 && len(markers) == 0 || len(numbers) != len(markers) {
		return
	}
	for i, marker := range markers {
		number, err := strconv.Atoi(numbers[i])
		if err != nil {
			return
		}
		value := time.Duration(number)
		switch marker {
		case "d":
			d += durationDay * value
		case "h":
			d += time.Hour * value
		case "m":
			d += time.Minute * value
		case "s":
			d += time.Second * value
		case "ms":
			d += time.Millisecond * value
		case "us":
			d += time.Microsecond * value
		case "ns":
			d += time.Nanosecond * value
		default:
			return
		}
	}
	if strings.HasPrefix(s, "-") {
		d = -d
	}
	ok = true
	return
}
