package konfig

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFieldBase64(t *testing.T) {
	b := Base64("123")

	assert.Equal(t, "123", b.String())
	assert.Equal(t, "123", b.GetString(false))
	assert.Equal(t, "MTIz", b.GetString(true))
	assert.Equal(t, "123", b.String())

	b.SetString("456", true)
	assert.Equal(t, "456", b.String())
	assert.Equal(t, "NDU2", b.GetString(true))

	b.SetString("MTIz", true)
	assert.Equal(t, "123", b.String())
}
