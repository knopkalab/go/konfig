module gitlab.com/knopkalab/go/konfig

go 1.17

require (
	github.com/stretchr/testify v1.7.0
	gitlab.com/knopkalab/go/utils v1.0.0
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
